class Point
{
    constructor(x,y)
    {
        this.x = x;
        this.y = y;
    }
}

class LabeledPoint
{
    constructor(point, label)
    {
        this.point = point;
        this.label = label;
    }
}

class Dataset
{
    constructor()
    {
        this.data = [];
    }

    add(labeledPoint)
    {
        this.data.push(labeledPoint);
    }

    get()
    {
        return this.data;
    }
};


class Canvas
{
    constructor(id)
    {
        this.cv = document.getElementById(id);
        this.ctx = this.cv.getContext("2d");

        this.scale = this.cv.width/10;
        this.offsetX = 5;
        this.offsetY = 5;

        this.colorScale = 10;
    }

    offsetScaleX(x)
    {
        return this.scale*(x + this.offsetX);
    }

    offsetScaleY(y)
    {
        return this.scale*(y + this.offsetY);
    }

    unOffsetScaleX(x)
    {
        return x/this.scale - this.offsetX;
    }

    unOffsetScaleY(y)
    {
        return y/this.scale - this.offsetY;
    }

    drawCircle(x,y,color)
    {
        x = this.offsetScaleX(x);
        y = this.offsetScaleY(y);

        let r = 5;
        this.ctx.strokeStyle = "#FFFFFF";
        this.ctx.fillStyle = color;
        this.ctx.beginPath();
        this.ctx.arc(x, y, r, 0, Math.PI*2, true);
        this.ctx.fill();
        this.ctx.stroke();
    }

    drawLine(x1,y1,x2,y2)
    {
        //Stroke color is black:
        this.ctx.strokeStyle = "#000000";
        this.ctx.beginPath();
        this.ctx.moveTo(x1,y1);
        this.ctx.lineTo(x2,y2);
        this.ctx.stroke();
    }

    clear()
    {
        this.ctx.clearRect(0, 0, this.cv.width, this.cv.height);
    }

    plotDataSet(dataset)
    {
        let data = dataset.get();

        for(let i=0; i<data.length; i++)
        {
            let point = data[i].point;
            this.drawCircle(point.x, point.y, data[i].label);
        }
    }

    plotFunction(f)
    {
        const colorScale = this.colorScale;

        //create empty image data:
        const w = this.cv.width;
        const h = this.cv.height;
        let imageData = this.ctx.createImageData(w, h);

        //for each pixel coordinates of the canvas:
        for (let j = 0; j < h; j++)
        {
            for (let i = 0; i < w; i++)
            {
                //Compute the x and y coordinates:
                const x = this.unOffsetScaleX(i);
                const y = this.unOffsetScaleY(j);

                //Compute the value of the function:
                const value = f.compute(x, y);

                //Compute the color:
                let r;
                let g;
                let b;

                if(value > 0)
                {
                    r = value*colorScale*255/2;
                    g = 0;
                    b = 0;

                    if(Math.abs(value) > 1)
                    {
                        r = 255;
                        b = 100;
                    }
                }
                else
                {
                    r = 0;
                    g = 0;
                    b = -value*colorScale*255/2;

                    
                    if(Math.abs(value) > 1)
                    {
                        b = 255;
                        r = 100;
                    }
                }

                if(Math.abs(value) <= 0.01)
                {
                    r = 255;
                    g = 255;
                    b = 255;
                }

                //Compute the pixel index:
                const index = (i + j * w) * 4;

                //Set the pixel value:
                imageData.data[index + 0] = r;
                imageData.data[index + 1] = g;
                imageData.data[index + 2] = b;
                imageData.data[index + 3] = 255;
            }
        }

        //draw the image data on the canvas:
        this.ctx.putImageData(imageData, 0, 0);
    }
}

function randomLineDataset()
{
    let dataset = new Dataset();

    let x0 = 7*(Math.random()-0.5);
    let y0 = 7*(Math.random()-0.5);
    let s0 = 1+Math.random()*2;
    let r0 = 0;

    for(let i=0; i<200; i++)
    {
        r = r0 + Math.random()*s0;
        a = Math.random()*2*Math.PI;

        let x = x0 + r*Math.cos(a);
        let y = y0 + r*Math.sin(a);

        dataset.add(new LabeledPoint(new Point(x, y), "red"));
    }

    
    let x1 = 7*(Math.random()-0.5);
    let y1 = 7*(Math.random()-0.5);
    let s1 = 1+Math.random()*2;
    let r1 = 0;

    for(let i=0; i<100; i++)
    {
        r = r1 + Math.random()*s1;
        a = Math.random()*2*Math.PI;

        let x = x1 + r*Math.cos(a);
        let y = y1 + r*Math.sin(a);
        dataset.add(new LabeledPoint(new Point(x, y), "blue"));
    }

    return dataset;
}


function randomCircleDataset()
{
    let dataset = new Dataset();

    let yscale = 1+(Math.random()-0.5);

    let x0 = 3*(Math.random()-0.5);
    let y0 = 3*(Math.random()-0.5);
    let s0 = 1+Math.random();
    let r0 = 0;

    for(let i=0; i<200; i++)
    {
        r = r0 + Math.random()*s0;
        a = Math.random()*2*Math.PI;

        let x = x0 + r*Math.cos(a);
        let y = y0 + r*Math.sin(a)*yscale;

        dataset.add(new LabeledPoint(new Point(x, y), "red"));
    }

    
    let x1 = x0+Math.random();
    let y1 = y0+Math.random();
    let s1 = 1+Math.random();
    let r1 = r0+2+Math.random();

    for(let i=0; i<100; i++)
    {
        r = r1 + Math.random()*s1;
        a = Math.random()*2*Math.PI;

        let x = x1 + r*Math.cos(a);
        let y = y1 + r*Math.sin(a)*yscale;
        dataset.add(new LabeledPoint(new Point(x, y), "blue"));
    }

    return dataset;
}


function randomPieDataset()
{
    let dataset = new Dataset();

    let rot = Math.random()*2*Math.PI;
    let yscale = 1+(Math.random()-0.5)/2;

    let x0 = 3*(Math.random()-0.5);
    let y0 = 3*(Math.random()-0.5);
    let s0 = 2+Math.random()*2;
    let r0 = Math.random();

    for(let i=0; i<200; i++)
    {
        r = r0 + Math.random()*s0;
        a = rot + (Math.random()*0.75)*2*Math.PI;

        let x = x0 + r*Math.cos(a);
        let y = y0 + r*Math.sin(a)*yscale;

        dataset.add(new LabeledPoint(new Point(x, y), "red"));
    }

    
    let x1 = x0;
    let y1 = y0;
    let s1 = s0;
    let r1 = r0;

    for(let i=0; i<100; i++)
    {
        r = r1 + Math.random()*s1;
        a = rot + (0.8+Math.random()*0.15)*2*Math.PI;

        let x = x1 + r*Math.cos(a);
        let y = y1 + r*Math.sin(a)*yscale;
        dataset.add(new LabeledPoint(new Point(x, y), "blue"));
    }

    return dataset;
}

function spiral(angle, radius, turns, angleOffset)
{


    let amp = radius*angle/(Math.PI*2*turns);

    let x = amp*Math.cos(angle*turns+angleOffset);
    let y = amp*Math.sin(angle*turns+angleOffset);

    return new Point(x,y);
}

function randomSpiralDataset()
{
    let dataset = new Dataset();

    let radius0 = (1+Math.random())*3;
    let turns0 = 1+Math.random();
    let angleOffset0 = Math.random()*Math.PI*2;

    for(let i=0; i<200; i++)
    {
        let angle = Math.random()*Math.PI*2;
        let p = spiral(angle, radius0, turns0, angleOffset0);

        p.x += (Math.random()-0.5)*0.5;
        p.y += (Math.random()-0.5)*0.5;

        dataset.add(new LabeledPoint(p, "red"));
    }

    let radius1 = radius0;
    let turns1 = turns0;
    let angleOffset1 = angleOffset0+Math.PI;

    for(let i=0; i<200; i++)
    {
        let angle = Math.random()*Math.PI*2;
        let p = spiral(angle, radius1, turns1, angleOffset1);

        p.x += (Math.random()-0.5)*0.5;
        p.y += (Math.random()-0.5)*0.5;


        dataset.add(new LabeledPoint(p, "blue"));
    }
 
    return dataset;
}




function evaluate(f, dataset)
{
    let data = dataset.get();

    let score = 0;
    for(let i=0; i<data.length; i++)
    {
        let point = data[i].point;
        let label = data[i].label;

        let predicted = f.compute(point.x, point.y);

        let expected = label == "red" ? 1 : -1;
        
        score += Math.abs(Math.pow(predicted - expected,2));
    }

    return score/data.length;
}

class Coefs
{
    constructor(nb)
    {
        this.coefs = [];

        for(let i=0;i<nb;i++)
            this.coefs.push(0);
    }

    clone()
    {
        let clone = new Coefs();

        for(let i=0;i<this.coefs.length;i++)
            clone.coefs[i] = this.coefs[i];
        
        return clone;
    }

    get(id)
    {
        return this.coefs[id];
    }
    
    randomScale(amp)
    {
        let clone = this.clone();

        for(let i=0;i<this.coefs.length;i++)
            clone.coefs[i] *= 1+(Math.random()-0.5)*amp;

        return clone;
    }

    randomShift(amp)
    {
        let clone = this.clone();

        for(let i=0;i<this.coefs.length;i++)
            clone.coefs[i] += (Math.random()-0.5)*amp;

        return clone;
    }

    randomAlter(amp)
    {
        return this.randomShift(amp).randomScale(amp);
    }
}

class LineFunction
{
    constructor()
    {
        this.coef = new Coefs(3);
    }

    updateCoef(coef)
    {
        this.coef = coef.clone();
    }

    getCoefs()
    {
        return this.coef.clone();
    }

    compute(x,y)
    {
        let a = this.coef.get(0);
        let b = this.coef.get(1);
        let c = this.coef.get(2);

        return a*x + b*y + c;
    }

    print()
    {
        let div = document.getElementById("function");
        div.innerHTML = "f(x₁,x₂) = " + this.coef.get(0).toFixed(2) + "x₁ + " + this.coef.get(1).toFixed(2) + "x₂ + " + this.coef.get(2).toFixed(2);
    }
}


class QuadFunction extends LineFunction
{
    constructor()
    {
        super();
        this.coef = new Coefs(6);
    }

    compute(x,y)
    {
        let a = this.coef.get(0);
        let b = this.coef.get(1);
        let c = this.coef.get(2);
        let d = this.coef.get(3);
        let e = this.coef.get(4);
        let f = this.coef.get(5);

        return a*x*x + b*y*y + c*x*y + d*x + e*y + f;
    }

    print()
    {
        let div = document.getElementById("function");
        div.innerHTML = "f(x₁,x₂) = " + this.coef.get(0).toFixed(2) + "x² + " + this.coef.get(1).toFixed(2) + "x₂² + " + this.coef.get(2).toFixed(2) + "x₁x₂ + " + this.coef.get(3).toFixed(2) + "x₁ + " + this.coef.get(4).toFixed(2) + "x₂ + " + this.coef.get(5).toFixed(2);
    }
}


class MultiLineFunction extends LineFunction
{
    constructor()
    {
        super();
        this.coef = new Coefs(13);
    }

    compute(x,y)
    {
        let a = this.coef.get(0);
        let b = this.coef.get(1);
        let c = this.coef.get(2);
        let d = this.coef.get(3);
        let e = this.coef.get(4);
        let f = this.coef.get(5);
        let g = this.coef.get(6);
        let h = this.coef.get(7);
        let i = this.coef.get(8);
        let j = this.coef.get(9);
        let k = this.coef.get(10);
        let l = this.coef.get(11);
        let m = this.coef.get(12);

        return j*Math.max(0, a*x + b*y + c) + k*Math.max(0, d*x + e*y + f) + l*Math.max(0, g*x + h*y + i) + m;

    }

    print()
    {
        let div = document.getElementById("function");
        div.innerHTML = "f(x₁,x₂) = " + this.coef.get(0).toFixed(2) + "max(0, " + this.coef.get(1).toFixed(2) + "x₁ + " + this.coef.get(2).toFixed(2) + "x₂ + " + this.coef.get(3).toFixed(2) + ") + " + this.coef.get(4).toFixed(2) + "max(0, " + this.coef.get(5).toFixed(2) + "x₁ + " + this.coef.get(6).toFixed(2) + "x₂ + " + this.coef.get(7).toFixed(2) + ") + " + this.coef.get(8).toFixed(2) + "max(0, " + this.coef.get(9).toFixed(2) + "x₁ + " + this.coef.get(10).toFixed(2) + "x₂ + " + this.coef.get(11).toFixed(2) + ") + " + this.coef.get(12).toFixed(2);
    }
}


class SingleLayerPerceptron extends LineFunction
{
    constructor()
    {
        super();
        this.nNeuronsLayer1 = 10;
        this.coef = new Coefs(this.nNeuronsLayer1*4+1);
    }

    compute(x,y)
    {
        let sum = 0;
        for(let i=0;i<this.nNeuronsLayer1;i++)
        {
            let a = this.coef.get(i*4+0);
            let b = this.coef.get(i*4+1);
            let c = this.coef.get(i*4+2);
            let d = this.coef.get(i*4+3);
            sum += d*Math.max(0, a*x + b*y + c);
        }

        return sum + this.coef.get(this.nNeuronsLayer1*4);
    }

    print()
    {
        let div = document.getElementById("function");
        div.innerHTML = "Single Layer Perceptron";
    }

}



class Perceptron
{
    constructor(fun, dataset)
    {
        this.cv = new Canvas("canvas");
        this.function = fun;
        this.function.updateCoef(this.function.getCoefs().randomAlter(0.1));
        this.dataset = dataset;
        this.coef = this.function.getCoefs();
        this.score = Infinity;
        this.rate = 0.02;
    }

    getCoefs()
    {
        return this.coef.clone();
    }

    backupCoefs()
    {
        this.coefBackup = this.coef.clone();
    }

    restoreCoefs()
    {
        this.coef = this.coefBackup.clone();
    }

    getFunction()
    {
        return this.function;
    }

    step()
    {
        this.cv.plotFunction(this.getFunction());
        this.cv.plotDataSet(this.dataset);
        
        this.getFunction().print();
        this.printScore();

        this.backupCoefs();

        for(let i=0;i<10000;i++)
        {
            this.coef = this.coef.randomAlter(this.rate);
            this.function.updateCoef(this.coef);

            let newScore = evaluate(this.getFunction(), this.dataset);
        
            if(newScore <= this.score)
            {
                this.score = newScore;
                break;
            }
            else
            {
                this.restoreCoefs();
                this.function.updateCoef(this.coef);
            }
        }

    }

  
    printScore()
    {
        let div = document.getElementById("score");
        div.innerHTML = "Score = " + this.score.toFixed(8);
    }

}

let perceptron;
let interval = null;

function step()
{
    perceptron.step();
}

function updatePerceptron() {
    const searchParams = new URLSearchParams(window.location.search);
    const type = searchParams.get("type");
    const dataset = searchParams.get("dataset");
    const colorScale = searchParams.get("colorScale");

    let functionObj;
    if (type === "quad") {
        functionObj = new QuadFunction();
    } else if (type === "multi")
    {
        functionObj = new MultiLineFunction();
    }
    else if(type === "singlelayer")
    {
        functionObj = new SingleLayerPerceptron();
    }
    else
    {
        functionObj = new LineFunction();
    }

    let datasetFn;
    if (dataset === "circle") {
        datasetFn = randomCircleDataset;
    } else if (dataset === "pie")
    {
        datasetFn = randomPieDataset;
    }
    else if(dataset === "spiral")
    {
        datasetFn = randomSpiralDataset;
    }
    else 
    {
        datasetFn = randomLineDataset;
    }

    
    perceptron = new Perceptron(functionObj, datasetFn());

    if (colorScale)
    {
        perceptron.cv.colorScale = colorScale;
    }

    perceptron.step();
}

updatePerceptron();

document.body.addEventListener("click", function()
{
    if(interval != null)
    {
        clearInterval(interval);
        interval = null;
        return;
    }

    interval = setInterval(step, 1000/60);
});

document.body.addEventListener("contextmenu", function(event) {
   
    event.preventDefault();
    updatePerceptron();
});

document.body.addEventListener("mousedown", function(event) {
    if (event.button === 1) {
        perceptron.rate = 0.1;
    }
});

document.body.addEventListener("mouseup", function(event) {
    if (event.button === 1) {
        perceptron.rate = 0.02;
    }
});
